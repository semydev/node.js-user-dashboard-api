import axios from 'axios';
import FormData from 'form-data';

import User from '../models/User.js';

export const redirect = async (req, res, next) => {
    try {
        const code = req.query.code || null;

        const form_data = new FormData();
        form_data.append('code', code);
        form_data.append('client_id', '907075034615.2212824743301');
        form_data.append('client_secret', '42ad705d0ccb6692df4e6c9ee71681f9');
        form_data.append('redirect_uri', 'https://localhost:8080/api/slack/redirect');

        const authed_user = await axios.post('https://slack.com/api/oauth.v2.access', 
            form_data, 
            { headers: form_data.getHeaders() }
        )
        .then(function (response) {
            return response.data.authed_user;
        })
        .catch(function (error) {
            console.log(error);
        });

        if (authed_user) {
            // Find user based on email
           const userExist = await User.findOne({ _id: req.user });
           if (!userExist) return res.status(401).json({ message: 'Account does not exist' });

           userExist.slackID = authed_user.id;
           userExist.slackToken = authed_user.access_token;
           await userExist.save();
       }
       return res.status(200);
    } catch (err) {
        console.log(err);
        res.status(500);
    }
};

export const getSlackUserInfo = async (req, res, next) => {
    try {
        const userExist = await User.findOne({ _id: req.user });
        if (!userExist) return res.status(401).json({ message: 'Account does not exist' });

        const form_data = new FormData();
        form_data.append('token', userExist.slackToken);
        form_data.append('user', userExist.slackID);
        
       return res.status(200).json({ 
           "id":userExist.slackID,
           "display_name":"stak",
           "avatar":"https://ca.slack-edge.com/T7J0AT6KG-U022EKP8LJH-3796f2aaa78d-72",
           "token_expiration":"2021-07-05T08:33:40.000Z",
           "updated_at":"2021-06-28T08:33:40.000Z",
        })
    } catch (err) {
        console.log(err);
        res.status(500).json(err);
    }
};
