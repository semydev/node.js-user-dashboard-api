import axios from 'axios';
import FormData from 'form-data';
import Stripe from 'stripe';
const stripe = new Stripe(process.env.STRIPE_SECRET_KEY);

import User from '../models/User.js';

export const getSubscriptionInfo = async (req, res, next) => {
    try {
        const userExist = await User.findOne({ _id: req.user });
        if (!userExist) return res.status(401).json({ message: 'Account does not exist', binded: false });
		if (userExist.slackID == "") return res.status(401).json({ message: 'Slack account has not been binded to dashboard account', binded: false });

        const email = "3519448999@qq.com"
        let { data: result } = await stripe.customers.list({ email: email, limit: 1 })
        if (result.length == 0) return res.status(401).json({ 
            message: 'You do not currently have an active membership. Please wait for one of our restocks to get access to our tools and group',
            expiration: null,
			binded: true, 
            status: "Not Active"
        });
            
        const customerId = result[0].id;
		const stripeEmail = result[0].email;
		const stripeName = result[0].name;

		const stripeCardId = result[0].sources.data[0].id;
		const stripeCardType = result[0].sources.data[0].brand;
		const stripeCardExpMonth = result[0].sources.data[0].exp_month;
		const stripeCardExpYear = result[0].sources.data[0].exp_year;
		const stripeCardLast4 = result[0].sources.data[0].last4;

		let stripeSubscriptionPeriodEnd = result[0].subscriptions.data[0].current_period_end;
		var formattedStripeSubscriptionPeriodEnd = new Date(0); // The 0 there is the key, which sets the date to the epoch
		formattedStripeSubscriptionPeriodEnd.setUTCSeconds(stripeSubscriptionPeriodEnd);

		let stripeSubscriptionStartDate = result[0].subscriptions.data[0].start_date;
		var formattedStripeSubscriptionStartDate = new Date(0); // The 0 there is the key, which sets the date to the epoch
		formattedStripeSubscriptionStartDate.setUTCSeconds(stripeSubscriptionStartDate);

		const stripePlanAmount = result[0].subscriptions.data[0].plan.amount;
		const stripePlanActive = result[0].subscriptions.data[0].plan.active;
		const stripePlanCurrency = result[0].subscriptions.data[0].plan.currency;

		return res.status(200).json({ 
            message: '',
			binded: true,
			subscription: {
				billing_email: stripeEmail,
				billing_name: stripeName,
				subscribed_since: formattedStripeSubscriptionStartDate,
				expiration: formattedStripeSubscriptionPeriodEnd,
				card_details: {
					card_id: stripeCardId,
					card_type: stripeCardType.toUpperCase(),
					exp_month: stripeCardExpMonth.toString(),
					exp_year: stripeCardExpYear.toString().substr(-2),
					last_4: stripeCardLast4.toString()
				},
				currency: stripePlanCurrency.toUpperCase(),
				price: `$${Number(stripePlanAmount/100).toFixed(2)}`,
				active_subscription: stripePlanActive ? true : false,
			},		
        });
    } catch (err) {
        console.log(err);
        res.status(500).json(err);
    }
};

export const upgradeSubscription = async (req, res, next) => {
    try {
        return res.status(200).json({message: "Reached Cancel Sub endpoint"})
     } catch (err) {
         console.log(err);
         res.status(500).json(err);
     }
};

export const cancelSubscription = async (req, res, next) => {
    try {
       return res.status(200).json({message: "Reached Cancel Sub endpoint"})
    } catch (err) {
        console.log(err);
        res.status(500).json(err);
    }
};




// creating new customer
export const newCustomer = async (req, res, next) => {
	try {
		const customersArray = await stripe.customers.list();
		const emails = customersArray.data.map(indvCustomer => indvCustomer.email);

		if (emails.includes(req.body.email)) {
			return res.status(500).send("Customer Already Exist !!");
		}

		const newCustomer = await stripe.customers.create({
			email: req.body.email,
			description: req.body.name,
			// source: "tok_visa",
		});

		// Create invoice items for subscription and create new invoice.
		stripe.invoiceItems.create(
			{
				amount: 7500,
				currency: "eur",
				description: "Subscribed",
				customer: newCustomer.id,
			}
			// (err, invoiceItems) => {
			// 	stripe.invoices.create({
			// 		customer: newCustomer.id,
			// 		auto_advance: false,
			// 	});
			// }
		);

		const editedUser = await User.findByIdAndUpdate(
			req.body.id,
			{ stripeCusID: newCustomer.id },
			{ new: true }
		);
		res.json({ editedUser });
	} catch (error) {
		res.status(422).json(error);
	}
};

// For adding items to invoicing
// Subscription & pay per purchase both can be added as invoice items
// POST /invoice/charge
export const AddItemsToInvoice = async (req, res, next) => {
	await stripe.invoiceItems
		.create({
			amount: 750,
			currency: "eur",
			description: "Event Purchased",
			customer: req.body.stripeCusID,
		})
		.then(status => res.json({ status }))
		.catch(err => res.status(500).json(err));
};

// Create a new invoice
// POST /invoice/createNew
export const CreateNewInvoice = async (req, res, next) => {
	await stripe.invoices.create({
				customer: req.body.stripeCusID,
				auto_advance: false,
			})
		.then(status => res.json({ status }))
		.catch(err => res.status(500).json(err));
};


// Retrive upcoming invoice
// POST /invoice/upcoming
export const UpcomingInvoice = async (req, res, next) => {
	await stripe.invoices
		.retrieveUpcoming(req.body.stripeCusID)
		.then(status => res.json({ status }))
		.catch(err => res.status(500).json(err));
};

//Finalize invoice
// POST /invoice/finalize
export const FinalizeInvoice = async (req, res, next) => {
	await stripe.invoices
		.finalizeInvoice(req.body.invoiceId)
		.then(status => res.json({ status }))
		.catch(err => res.status(500).json(err));
};


//Pay invoice
// POST /invoice/pay
export const PayInvoice = async (req, res, next) => {
	await stripe.invoices
		.pay(req.body.invoiceId)
		.then(status => res.json({ status }))
		.catch(err => res.status(500).json(err));
};