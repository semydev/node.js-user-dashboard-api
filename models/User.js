import mongoose from 'mongoose';

const instance = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      min: 6,
      max: 255,
    },
    password: {
      type: String,
      required: true,
      min: 8,
      max: 1024,
      select: false,
    },
    billingID: {
      type: String,
      min: 6,
      max: 255,
    },
    slackID: {
      type: String,
      min: 10,
      max: 255,
    },
    slackToken: {
      type: String,
      min: 50,
      max: 255,
    },
    firstName: {
      type: String,
      required: true,
      max: 255,
    },
    lastName: {
      type: String,
      required: true,
      max: 255,
    },
    hasTrial: { 
      type: Boolean, 
      default: false 
    },
    endDate: { 
      type: Date, 
      default: null 
    },
  },
  {
    timestamps: true
  }
)

const modelName = 'User';

export default mongoose.model(modelName, instance);