import jwt from 'jsonwebtoken';

const secret = process.env.JWT_SECRET || 'secret';

export const generateAuthToken = (id) => {
    return jwt.sign({ id }, new Buffer.from(secret, 'base64'), { expiresIn: '1h' });
};

//TODO: Implement to way to check if token is valid but has expired
export const authenticateToken = (req, res, next) => {
    try {
        const token = req.headers.authorization.split('Bearer ')[1];
        if (!token) return res.status(401).json({ message: 'No Authentication token provided' });

        const decoded = jwt.verify(token, new Buffer.from(secret, 'base64'));
        if (!decoded) return res.status(401).json({ message: 'Authentication failed, access denied' });
    
        req.user = decoded.id;
        next()
    } catch (err) {
        console.log(err)
        return res.status(500).send({ message: 'Failed to authenticate token' });
    }
};

export const authenticateTokenCookie = (req, res, next) => {
    try {
        const token = req.cookies.jwt;
        if (!token) return res.status(401).json({ message: 'No Authentication token provided' });

        const decoded = jwt.verify(token, new Buffer.from(secret, 'base64'));
        if (!decoded) return res.status(401).json({ message: 'Authentication failed, access denied' });
    
        req.user = decoded.id;
        next()
    } catch (err) {
        console.log(err)
        return res.status(500).send({ message: 'Failed to authenticate token' });
    }
};