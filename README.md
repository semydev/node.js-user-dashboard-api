# Node.js User Dashboard API

This project serves as the primary API for a custom built user subscription dashboard built for a client. This API powers a react user management dashboard where users can login and few their subscription to a specific service and manage that subscription. There are numerous specific business logic functions, such as integrations with Slack, integration with Stripe and so forth. This implements JWT authentication and is very much still a WIP for the next month before it is signed off and delivered to the client. 

