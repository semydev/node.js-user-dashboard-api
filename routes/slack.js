import express from 'express';
import { redirect, getSlackUserInfo } from '../controllers/slack.js'; // import request & response function

import { authenticateToken, authenticateTokenCookie } from '../middleware/authToken.js';

// initialize router
const router = express.Router();

// Login related routes
router.get('/redirect', authenticateTokenCookie, redirect);
router.get('/me', authenticateToken, getSlackUserInfo);

export default router;