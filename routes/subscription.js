import express from 'express';
import { getSubscriptionInfo, upgradeSubscription, cancelSubscription } from '../controllers/subscription.js'; // import request & response function

import { authenticateToken } from '../middleware/authToken.js';

// initialize router
const router = express.Router();

// Login related routes
router.get('/me', authenticateToken, getSubscriptionInfo);
router.get('/upgrade', authenticateToken, upgradeSubscription);
router.get('/cancel', authenticateToken, cancelSubscription);

export default router;