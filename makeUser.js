import bcrypt from "bcryptjs";
const SALT = await bcrypt.genSalt(10)
const encryptedPassword = await bcrypt.hash("Ab123456", SALT)
console.log(encryptedPassword);